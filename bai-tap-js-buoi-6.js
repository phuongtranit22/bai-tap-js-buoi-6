
function timSoNguyenDuong(n) { // truyền tham số cho linh hoạt, ko cố định 10,000 như đề bài
    var tong = 0;
    var i;
    for (i = 1; tong <= n; i++) { // i chạy từ 1 tới khi tổng chưa lớn hơn 10,000 => tổng từ 10,000 trở xuống
        tong += i;
    }
    return --i; // phải lùi lại 1 số mới đúng
}
// console.log('Cấp số cộng đến n')
// console.log(timSoNguyenDuong(10000));
// console.log('-----------')
// console.log(timSoNguyenDuong(0)); // chỉ cần con số 1 là đủ
// console.log(timSoNguyenDuong(1)); // phải cộng thêm con số 2 thì mới > 1 được
// console.log(timSoNguyenDuong(2)); // 2 thì cũng vậy
// console.log('-----------')
// console.log(timSoNguyenDuong(3)); // 1 + 2 = 3 Mà 3 thì đâu có lớn hơn 3, phải cộng thêm con số tiếp theo, là số 3 đó
// console.log(timSoNguyenDuong(4)); // 1 + 2 + 3 = 6 lận, nên số 3 xài dài dài tới 6 lận mới đổi lên số 4
// console.log(timSoNguyenDuong(5)); // (1 + 2 + 3 = 6) > 5 Vậy số cần tìm là 3
// console.log(timSoNguyenDuong(6)); // (1 + 2 + 3 + 4 = 10) > 6 Vậy số cần tìm là 4 nhé

///////////////////////////////
function tinhTongTangDanLuyThua(x, n) {
    var tongCapSoCong = 0;
    var i;
    for (i = 1; i <= n; i++) { // i chạy từ 1 tới BẰNG n
        tongCapSoCong += Math.pow(x, i); // x^1 + x^2 + ... + x^n
    }
    return tongCapSoCong;
}
console.log('Tổng tăng dần luỹ thừa')
console.log(tinhTongTangDanLuyThua(2, 3)) // 2 + 2^2 + 2^3 = 14


////////////////////////////////
function tinhGiaiThua(n) {
    var ketQua = 1;
    var i;
    for (i = 1; i <= n; i++) {
        ketQua *= i
    }
    return ketQua
}
console.log('Giai thừa')
console.log(tinhGiaiThua(4)) // 1*2*3*4 = 24


///////////////////////////////
function inDiv(n, container) {
    var ketQua = ''
    for (var i = 1; i <= n; i++) {
        if (i % 2 === 0) { // số chẵn
            ketQua += `<div style="background: red; width: 70px; height: 20px"></div>`
        } else {
            ketQua += `<div style="background: blue; width: 70px; height: 20px"></div>`
        }
    }
    container.innerHTML = ketQua;
}

//////////////////////////////////////////////
// show lên giao diện
function bai1() {
    document.getElementById('bai1').innerText = timSoNguyenDuong(10000);
}

function bai2() {
    var x2 = document.getElementById('x2').value * 1;
    var n2 = document.getElementById('n2').value * 1;
    document.getElementById('bai2').innerText = tinhTongTangDanLuyThua(x2, n2);
}

function bai3() {
    var n3 = document.getElementById('n3').value * 1;
    document.getElementById('bai3').innerText = tinhGiaiThua(n3);
}

function bai4() {
    inDiv(10, document.getElementById('bai4'));
}
